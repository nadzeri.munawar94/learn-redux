export const FETCH = 'user/FETCH';
export const FETCH_SUCCESS = 'user/FETCH_SUCCESS';
export const FETCH_FAIL = 'user/FETCH_FAIL';