export const FETCH = 'user-list/FETCH';
export const FETCH_SUCCESS = 'user-list/FETCH_SUCCESS';
export const FETCH_FAIL = 'user-list/FETCH_FAIL';