import { FETCH, FETCH_SUCCESS, FETCH_FAIL } from "../actions/user-list";

const initialState = {
  state: null,
  loading: false,
  users: [],
  error: null,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case FETCH: {
      return {
        ...state,
        loading: true,
        state: "LOADING",
      };
    }

    case FETCH_SUCCESS: {
      return {
        ...state,
        loading: false,
        state: "SUCCESS",
        users: action.payload,
        error: null
      };
    }

    case FETCH_FAIL: {
        return {
          ...state,
          loading: false,
          state: "ERROR",
          users: [],
          error: action.payload
        };
      }

    default: {
      return state;
    }
  }
}
