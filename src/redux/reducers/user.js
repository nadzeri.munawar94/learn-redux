import { FETCH, FETCH_SUCCESS, FETCH_FAIL } from "../actions/user";

const initialState = {
  loading: false,
  users: [],
  error: null,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case FETCH: {
      return {
        ...state,
        loading: true,
      };
    }

    case FETCH_SUCCESS: {
      return {
        ...state,
        loading: false,
        users: action.payload,
      };
    }

    case FETCH_FAIL: {
        return {
            ...state,
            loading: false,
            error: action.error
        }
      }

    default: {
      return state;
    }
  }
}
