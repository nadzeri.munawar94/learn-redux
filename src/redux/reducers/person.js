import { ADD, SAVE_FORM_DATA } from "../actions/person";

const initialState = {
  data: [
    {
      name: "User 1",
      email: "user1@gmail.com",
    },
  ],
  formData: {
    name: "",
    email: ""
  }
};

export default function (state = initialState, action) {
  switch (action.type) {
    case ADD: {
      return {
        ...state,
        data: [...state.data, state.formData]
      };
    }

    case SAVE_FORM_DATA: {
      return {
        ...state,
        formData: { ...state.formData, ...action.payload }
      }
    }

    default: {
      return state;
    }
  }
}
