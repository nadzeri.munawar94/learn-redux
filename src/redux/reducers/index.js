import { combineReducers } from "redux";
import persons from "./person";
import users from "./user";
import userList from "./user-list";

export default combineReducers({
  persons,
  users,
  userList
});
