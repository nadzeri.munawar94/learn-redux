import { configureStore } from '@reduxjs/toolkit'
import reducers from './reducers';

// export default createStore(reducers, applyMiddleware(thunk));

export default configureStore({
    reducer: reducers
})