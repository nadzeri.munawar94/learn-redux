import { FETCH, FETCH_SUCCESS, FETCH_FAIL } from "../redux/actions/user-list";

export function fetchUsers() {
  return async function fetchUsersThunk(dispatch, getState) {
    dispatch({ type: FETCH });
    try {
      const response = await fetch("https://reqres.in/api/users?page=2");
      const result = await response.json();

      setTimeout(() => {
        dispatch({
          type: FETCH_SUCCESS,
          payload: result.data,
        });
      }, 1000);
    } catch (err) {
      dispatch({
        type: FETCH_FAIL,
        payload: err.message,
      });
    }
  };
}
