import { FETCH_SUCCESS } from '../redux/actions/user';

export function fetchUsers() {
  return async function fetchUsersThunk(dispatch, getState) {
    const response = await fetch("https://dummyjson.com/products");
    const result = await response.json();
    console.log('data already fetched')
    dispatch({
      type: FETCH_SUCCESS,
      payload: result.products
    });
  };
}
