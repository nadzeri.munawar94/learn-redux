import { useEffect, useState } from "react";

export default function PopularProduct() {
  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  useEffect(async () => {
    
    try {
        setLoading(true);

        const response = await fetch("http://localhost/popular-product");
        const data = await response.json();
    
        setLoading(false);
        setProducts(data);
    } catch(err) {
        setLoading(false);
        setError(err);
    }
    
  }, []);

  return (
    <>
      {error ? (
        <>Ada error dengan pesan {error}. Klik tombol ini untuk refresh data</>
      ) : (
        products.map((product) => {
          return (
            <ProductCard loading={loading} product={product}></ProductCard>
          );
        })
      )}
    </>
  );
}
