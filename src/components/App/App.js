import UserListRedux from '../UserListRedux/UserListRedux';
import './App.css';

function App() {
  return (
    <div className="App">
      <UserListRedux></UserListRedux>
    </div>
  );
}

export default App;
