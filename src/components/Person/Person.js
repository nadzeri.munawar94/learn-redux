import { Component } from "react";
import { connect } from "react-redux";
import { ADD, SAVE_FORM_DATA } from "../../redux/actions/person";

class Person extends Component {
  handleFormChange(e) {
    this.props.dispatch({
      type: SAVE_FORM_DATA,
      payload: {
        [e.target.name]: e.target.value
      },
    });
  }

  handleSubmit(e) {
    e.preventDefault();

    this.props.dispatch({
      type: ADD
    });
  }

  render() {
    return (
      <div>
        <h1>Person component</h1>
        <ol>
          {this.props.personState.data.map((person, index) => {
            return (
              <li key={index}>
                {person.name}, {person.email}
              </li>
            );
          })}
        </ol>
        <form onSubmit={this.handleSubmit.bind(this)}>
          <div>
            <label>Name</label>
            <input
              type="text"
              name="name"
              onChange={this.handleFormChange.bind(this)}
            />
          </div>
          <div>
            <label>Email</label>
            <input
              type="email"
              name="email"
              onChange={this.handleFormChange.bind(this)}
            />
          </div>
          <button type="submit">Add User</button>
        </form>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  personState: state.persons,
});

export default connect(mapStateToProps)(Person);
