import loading from "../../assets/gif/loading.gif"

export default function Loading() {
    return (
        <img src={loading}></img>
    )
}