import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { fetchUsers } from '../../middlewares/user';

export default function User() {
  const userState = useSelector((state) => state.users);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchUsers())
  }, []);

  return (
    <div>
      <h1>User component</h1>
      <ol>
        {userState.users.map((user) => {
          return (
            <li key={user.id}>
              {user.title}
            </li>
          );
        })}
      </ol>
    </div>
  );
}
