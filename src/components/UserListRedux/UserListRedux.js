import { useDispatch, useSelector } from "react-redux";
import Loading from "../Loading/Loading";
import { fetchUsers } from "../../middlewares/user-list";

export default function UserListRedux() {
  const userListState = useSelector((state) => state.userList);
  const dispatch = useDispatch();

  const fetchData = () => {
    dispatch(fetchUsers());
  }

  const renderState = () => {
    switch (userListState.state) {
      case "LOADING": {
        return (
          <div>
            <Loading></Loading>
          </div>
        );
      }

      case "SUCCESS": {
        return userListState.users.map((user) => {
          return (
            <div key={user.id}>
              <img src={user.avatar} height="100" />
              <div>Nama: {user.first_name + " " + user.last_name}</div>
              <div>email: {user.email}</div>
              <div>email: {user.email}</div>
            </div>
          );
        });
      }

      case "ERROR": {
        return (
          <div>
            Ada error pada saat request, mohon fetch data kembali. Pesan
            errornya adalah: {userListState.error}
          </div>
        );
      }

      default: {
        return;
      }
    }
  };

  return (
    <div>
      <h1>User list component from redux</h1>
      <button onClick={fetchData}>Fetch Data User</button>
      <br />
      <br />
      <br />
      {renderState()}
    </div>
  );
}
