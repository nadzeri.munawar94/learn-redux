import { useState } from "react";
import Loading from "../Loading/Loading";

export default function UserList() {
  const [state, setState] = useState(); // LOADING, SUCCESS, ERROR
  const [loading, setLoading] = useState(false);
  const [users, setUsers] = useState([]);
  const [error, setError] = useState(null);

  const fetchData = () => {
    setLoading(true);
    setState("LOADING");

    fetch("https://reqres.in/api/users?page=2")
      .then((response) => {
        response
          .json()
          .then((result) => {
            setTimeout(() => {
              setUsers(result.data);
              setLoading(false);
              setError(null);
              setState("SUCCESS");
            }, 1000);
          })
          .catch((err) => {
            setLoading(false);
            setError(err.message);
            setState("ERROR");
          });
      })
      .catch((err) => {
        setLoading(false);
        setError(err.message);
        setState("ERROR");
      });
  };

  const renderState = () => {
    switch (state) {
      case "LOADING": {
        return (
          <div>
            <Loading></Loading>
          </div>
        );
      }

      case "SUCCESS": {
        return users.map((user) => {
          return (
            <div key={user.id}>
              <img src={user.avatar} height="100" />
              <div>Nama: {user.first_name + " " + user.last_name}</div>
              <div>email: {user.email}</div>
              <div>email: {user.email}</div>
            </div>
          );
        });
      }

      case "ERROR": {
        return (
          <div>
            Ada error pada saat request, mohon fetch data kembali. Pesan
            errornya adalah: {error}
          </div>
        );
      }

      default: {
        return;
      }
    }
  };

  return (
    <div>
      <h1>User list component</h1>
      <button onClick={fetchData}>Fetch Data User</button>
      <br />
      <br />
      <br />
      {renderState()}
    </div>
  );
}
